const TRACK_NEW = 'TRACK_NEW'

export const newTrack = (track) => ({
  type: TRACK_NEW, track
})

const initialState = { track : null }
export const trackReducer = (state = initialState, action) => {
  switch(action.type) {
    case TRACK_NEW: return {
      ...state, 
     track: action.track
    }
    default: return state
  }
}
