import ApiConfig from './ApiConfig'

const RANKING_NEW_REQUEST  = 'RANKING_NEW_REQUEST'
const RANKING_NEW_ERROR    = 'RANKING_NEW_ERROR'
const RANKING_NEW_SUCCESS  = 'RANKING_NEW_SUCCESS'

export const getRanking = () => async (dispatch) => {
  dispatch({ type: RANKING_NEW_REQUEST})

  try {
    const response = await fetch(`${ApiConfig.url}/ranking`)
    const json = await response.json()
    dispatch({ type: RANKING_NEW_SUCCESS, data: json.data })
  } catch (error) {
    dispatch({ type: RANKING_NEW_ERROR, error })
  }
}

const initialState = { data: [], loading: false, error: false }
export const rankingReducer = (state = initialState, action) => {
  switch(action.type) {
    case RANKING_NEW_REQUEST: return {...state, loading: true}
    case RANKING_NEW_ERROR: return {...state, loading: false, error: action.error}
    case RANKING_NEW_SUCCESS: return {...state, loading: false, data: action.data}
    default: return state
  }
}
