const TIMER_START = 'TIMER_START'
const TIMER_STOP  = 'TIMER_STOP'
const TIMER_TICK  = 'TIMER_TICK'

let timer = null;
export const start = () => async (dispatch) => {
  timer && clearInterval(timer)
  timer = setInterval(() => dispatch(tick()), 1000)
  dispatch({ type: TIMER_START, timer: timer })
  dispatch({ type: TIMER_TICK })
}

const tick = () => ({ type: TIMER_TICK })

export const stop = () => {
  clearInterval(timer)
  return { type: TIMER_STOP }
}

const initialState = { total: 60*5, tick: 60*5, running: false, timer: null }
export const timerReducer = (state = initialState, action) => {
  switch(action.type) {
    case TIMER_START: return {
      ...state, 
      timer: action.timer
    }
    case TIMER_STOP: return {...state, running: false, timer: null}
    case TIMER_TICK: return {...state, tick: state.tick - 1}
    default: return state
  }
}
