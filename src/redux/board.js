import ApiConfig from './ApiConfig'

const BOARD_NEW_REQUEST  = 'BOARD_NEW_REQUEST'
const BOARD_NEW_SUCCESS  = 'BOARD_NEW_SUCCESS'
const BOARD_NEW_ERROR    = 'BOARD_NEW_ERROR'
const BOARD_CELL_CLICKED = 'BOARD_CELL_CLICKED'
const BOARD_LOSE         = 'BOARD_LOSE'
const BOARD_WIN          = 'BOARD_WIN'
const BOARD_SAVE_REQUEST = 'BOARD_SAVE_REQUEST'
const BOARD_SAVE_SUCCESS = 'BOARD_SAVE_SUCCESS'
const BOARD_SAVE_ERROR   = 'BOARD_SAVE_ERROR'

export const getNewBoard = () => async (dispatch) => {
  dispatch({ type: BOARD_NEW_REQUEST })

  try {
    const response = await fetch(`${ApiConfig.url}/board?width=10&height=10&mines=10`)
    const json = await response.json()
    const data = json.data.map(row => (
      row.map(cell => ({
        value: cell,
        clicked: false,
        minesAround: 0,
      }))
    ))
    dispatch({ type: BOARD_NEW_SUCCESS, data, lackToDiscover: (10*10)-10 })
  } catch (error) {
    dispatch({ type: BOARD_NEW_ERROR, error })
  }
}

export const discoverCell = (x,y, minesAround) => ({ type: BOARD_CELL_CLICKED, position: {x,y}, minesAround })

export const lose = () => ({ type: BOARD_LOSE })

export const win = () => ({ type: BOARD_WIN })

export const save = (data) => async (dispatch) => {
  dispatch({ type: BOARD_SAVE_REQUEST })
  try {
    const response = await fetch(`${ApiConfig.url}/gamer`, {
      headers: {
      'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'PUT', body: JSON.stringify({data})
    })
    const json = await response.json()
    dispatch({ type: BOARD_SAVE_SUCCESS, data: json.data })
  } catch (error) {
    dispatch({ type: BOARD_SAVE_ERROR, error })
  }
}


const initialState = { data: [], loading: false, error: false, lose: false }
export const boardReducer = (state = initialState, action) => {
  switch(action.type) {
    case BOARD_NEW_REQUEST: return {...state, loading: true}
    case BOARD_NEW_ERROR: return {...state, loading: false, error: action.error}
    case BOARD_NEW_SUCCESS: return {...state, loading: false, data: action.data, lackToDiscover: action.lackToDiscover}
    case BOARD_CELL_CLICKED:
      const {minesAround, position} = action
      const {x, y} = position
      const newData = state.data.map((row, i) => (
        row.map((cell, j) => {
          if ((x === i) && (y === j)) {
            return {...cell, clicked: true, minesAround}
          }
          return cell
        })
      ))
      return {...state, data: newData, lackToDiscover: state.lackToDiscover -1}
    case BOARD_LOSE: return {...state, lose: true}
    default: return state
  }
}
