import React, { Component } from 'react';
import { connect } from 'react-redux';
import { start, stop } from '../redux/timer';


class Timer extends Component {
  componentDidMount() {
    this.props.start()
  }
  render() {
    const {tick} = this.props
    return (
      <div className="timer">
        Time: {tick}
      </div>
    )
  }
}

export default connect(
  (state) => ({tick: state.timer.tick}),
  {start, stop}
)(Timer);
