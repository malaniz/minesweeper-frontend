import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getNewBoard, discoverCell, lose, win, save } from '../redux/board';
import { newTrack } from '../redux/track';

class Board extends Component {

  constructor(props) {
    super(props)
    this.state = {
      inputText: ""
    }
  }

  componentDidMount() {
    this.props.getNewBoard()
  }

  handleCell(curX, curY) {
    const {data, lackToDiscover} = this.props.board;
    let minesAround = 0;
    for (let x= -1; x <= 1; x++) {
      for (let y= -1; y <= 1; y++) {
        const tmpX = curX + x;
        const tmpY = curY + y;

        if (
          (tmpX >= 0) && (tmpX < data.length) &&
          (tmpY >= 0) && (tmpY < data[0].length)
        ) {
          minesAround += data[tmpX][tmpY].value
        }
      }
    }
    let message = ""
    if (data[curX][curY].value === 1) {
      this.props.lose()
      message = "BOOM!!!"
    } else {
      message = `Mines around ${minesAround}`
    }
    this.props.discoverCell(curX, curY, minesAround);
    if (lackToDiscover === 0) {
      this.props.win()
      message = 'You win!!!'
    }
    this.props.newTrack(message)
  }

  handleSubmit(e) {
    console.log('sending ...', this.state.inputText)
    this.props.save({
      name: this.state.inputText,
      score: this.props.time,
    })
  }

  handleInputChange(e) {
    this.setState({inputText: e.target.value})
  }

  render() {
    const { data, loading, lose, lackToDiscover } = this.props.board
    if (loading) return <div>
      <h1> Loading Board ... </h1>
    </div>
    
    if (lose) return <div>
      <h1> You lose! </h1>
    </div>

    if (lackToDiscover === 0) return <div>
      <h1> You Win! </h1>
      <p> Please insert your name to save your score </p>
      <form>
        <input type="text" name="gamerName" onChange={(e) => this.handleInputChange(e)} />
        <button onClick={(e) => {
          e.preventDefault()
          this.handleSubmit()
        }}> Save your Score! </button>
      </form>
    </div>

    return (
      <div className="board">
      {
        data.map((row, i) => (
          <div
            key={`board-row-${i}`}
            className="row"
          >
            {
              row.map((cell, j) => (
                <div
                  key={`board-cell-${i}-${j}`}
                  className="cell"
                  onClick={(e) => {
                    this.handleCell(i,j)
                  }}
                >
                {
                  cell.clicked? (
                    <span>
                      {cell.value}
                      <sub>{cell.minesAround}</sub>
                    </span>
                  ) : '--'
                }
                </div>
              ))
            }
          </div>
        ))
      }
      </div>
    )
  }
}

export default connect(
  (state) => ({board: state.board, time: state.timer.tick}),
  {
    getNewBoard,
    newTrack,
    discoverCell,
    lose,
    win,
    save,
  },
)(Board);
