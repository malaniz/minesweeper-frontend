import React, { Component } from 'react';
import { connect } from 'react-redux';

class Track extends Component {
  render() {
    const {track} = this.props
    if (track !== null) {
      return (
        <div>
            Track: {track}
        </div>
      )
    }
    return <div />
  }
}

export default connect(
  (state) => ({track: state.track.track}),
  {}
)(Track);
