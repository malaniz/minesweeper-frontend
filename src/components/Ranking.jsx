import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getRanking } from '../redux/ranking';

class Ranking extends Component {

  componentDidMount() {
    this.props.getRanking()
  }

  render() {
    const {ranking} = this.props
    return (
      <div className="ranking">
        <h3> Ranking</h3>
        <ul>
          {
            ranking.map((gamer, i) => (
              <li key={`ranking-item-${i}`}>
                {gamer.name}
              </li>
            ))
          }
        </ul>
      </div>
    )
  }
}

export default connect(
  (state) => ({ranking: state.ranking.data}),
  {getRanking}
)(Ranking);
