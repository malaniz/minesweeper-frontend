import React, { Component } from 'react';
import Board from './Board'
import Timer from './Timer'
import Track from './Track'
import Ranking from './Ranking'

export default class Game extends Component {
  render() {
    return (
      <div>
        <Timer />
        <Board />
        <Track />
        <Ranking />
      </div>
    )
  }
}
