import React, { Component } from 'react';

import {
  createStore,
  combineReducers,
  applyMiddleware,
  compose,
} from 'redux'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

import Game from './components/Game'

import { boardReducer } from './redux/board'
import { timerReducer } from './redux/timer'
import { trackReducer } from './redux/track'
import { rankingReducer } from './redux/ranking'


import './App.css'

const rootReducer = combineReducers({
  board: boardReducer,
  timer: timerReducer,
  track: trackReducer,
  ranking: rankingReducer,
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)))

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Game />
      </Provider>
    )
  }
}

export default App;
